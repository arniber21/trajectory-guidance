import numpy as np
import pandas as pd
import sklearn as sk

# Load data from csv
def load_data(filename):
    print("TODO")

# Clean and format data from csv
def clean_data(data):
    print("TODO")

# Create the regression model
def create_model(data): 
    print("TODO")

# Test and fit the regression model
def train_model(model): 
    print("TODO")

# Visualize the completed model
def visualize_model(model):
    print("TODO")

# Create launch tables
def launch_tables(model):
    print("TODO")