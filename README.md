# Trajectory Guidance

Trajectory Guidance is a Python program that outputs the suggested angle the hammer should be oriented at to hit targets at a user-provided range in meters. It utilizes Linear Regression (at the moment) with existing training data to do so. 

In the future, we plan to provide enough abstraction in order to leverage the power of guidance for other creations requiring precision to aim and operate. 

For more complex contraptions, we plan to allow the usage of other more advanced machine learning models in order to predict the curve, which would allow us to use this program on non-linear or non-logistic curves. 